<?php
/*DefaultController.php
Admin bundle for Symfony template
Copyright (C) 2016,2017 Daniel Fredriksen
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


namespace CYINT\ComponentsPHP\Bundles\AdminBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use CYINT\ComponentsPHP\Classes\ViewMessage;
use CYINT\ComponentsPHP\Classes\ParseData;
use CYINT\ComponentsPHP\Bundles\SettingsBundle\Entity\Setting;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException; 
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\HttpFoundation\Session\Session;
use Doctrine\Common\Collections\ArrayCollection;
use CYINT\ComponentsPHP\Controller\MasterController;

class DefaultController extends MasterController
{
    public function universalDeleteAction(Request $Request, $reponame, $id)
    {
        $Response = new JsonResponse();
		$reponame = stristr($reponame, ':') > -1 ? $reponame : "AppBundle:$reponame";

		if($Request->isMethod('POST'))
		{
			$Doctrine = $this->get('doctrine');
			//TODO make ApBundle dynami
			$Repository = $Doctrine->getRepository($reponame);
			$Entity = $Repository->find($id);
			if(empty($Entity))
				throw new \Exception('Page not found', 404);     
			$EntityManager = $Doctrine->getManager();
			$EntityManager->remove($Entity);
			$EntityManager->flush();      
			$Response->setData(['code'=>'200']);
		}
		else
			$Response->setData(['code'=>'403']);       

        return $Response;   
    }

    public function universalListAction(Request $Request, $reponame, $id = null)
    {
        $query_data = $Request->query->all();
        $filter = ParseData::setArray($query_data, 'filter', null);
		$reponame = stristr($reponame, ':') > -1 ? $reponame : "AppBundle:$reponame";
		$repopart = explode(':', $reponame);

        if($Request->isMethod('POST'))
        {
            $form_data = $Request->request->all();
            $filter = ParseData::setArray($form_data,'filter',null);
        }

        $Doctrine = $this->get('doctrine');
        $Repository = $Doctrine->getRepository($reponame);
        if(empty($id))
            $entity_list = $Repository->findByFilter($filter);
        else
            $entity_list = $Repository->findByFilter($filter, $id);

        $Cryptography = $this->get('app.cryptography');

        return $this->render("admin/" . $repopart[1] . "/list.html.twig",[
            'entity_list' => $entity_list,
            'filter' => $filter,
            'reponame' => $reponame,
            'Cryptography' => $Cryptography,
            'id' => $id
        ]);
    }

    public function universalFormAction(Request $Request, $id = null, $parentid = null, $reponame)
    {     
         return $this->handleErrors(
            function ($Session, $messages) use ($Request, $id, $reponame, $parentid)
            {
				$reponame = stristr($reponame, ':') > -1 ? $reponame : "AppBundle:$reponame";
				$repopart = explode(':', $reponame);

                $_locale = $Request->getLocale();
                $Doctrine = $this->getDoctrine();
                $Repository = $Doctrine->getRepository($reponame);
                $Factory = $Repository->getFactory($Doctrine, $this->container);         
                $fields = [];
                $Entity = null;

                if(!empty($id))
                {    
                    $Entity = $Repository->find($id);
           
                    if(empty($Entity))
                        return $this->redirect($this->generateUrl('404'));
                }

                if($Request->isMethod('POST'))
                {
                    $form_data = $Request->request->all();      
                    $this->universalFormProcessing($Factory, $form_data, $_locale, $Entity, $parentid);            
                    $messages[] = ViewMessage::constructMessage($Factory->getSuccessMessage(empty($Entity)), 'success');
                    $Session->set('messages', $messages);
                    return $this->redirect($this->generateUrl('admin_universal_list', ['filter'=>null, 'reponame'=>$reponame, 'id'=>$parentid]));                   
                }
                else               
                    $Factory->prepareEntityFieldData($_locale, $Entity, $parentid);                

                $Factory->prepareUniqueData();  

                return $this->render("admin/" . $repopart[1] . "/content.html.twig", array(              
                    'fields' => $Factory->getFields()
                    ,'settings' => $Factory->getSettings()
                    ,'Entity' => $Entity
                    ,'create' =>  empty($id) ? true : false
                    ,'_locale' => $_locale
                    ,'parentid' => $parentid
                    ,'reponame' => $reponame
                ));
            }
            ,$this->generateUrl($Request->get('_route'), ['id'=>$id, 'reponame'=>$reponame, 'parentid'=>$parentid])
         );
    }
 
    private function universalFormProcessing($Factory, $form_data, $_locale = 'en', $Entity = null, $parentid)
    {       
        try
        {
            $Doctrine = $this->get('doctrine');
            $EntityManager = $Doctrine->getManager();
            $fields = $Factory->constructEntityFromData($form_data, $Entity, $_locale, $parentid); 
            $Factory->persistData($Entity);
            $Factory->postProcessing($EntityManager, $Entity, $fields);
        }
        catch(UniqueConstraintViolationException $Ex)
        {
            throw new \Exception($Factory->getExceptionMessage($Ex));           
        }
        catch(\Exception $Ex)
        {
            throw new \Exception($Factory->getExceptionMessage($Ex));
        }
    }

    public function settingsAction(Request $Request, $template="layout")
    {
        return $this->handleErrors(
            function ($Session, $messages) use ($Request, $template)
            {

                $Doctrine = $this->getDoctrine();
                $settings = $Doctrine->getRepository('CYINTSettingsBundle:Setting')->findAll();
                $settings_array = [];
                foreach($settings as $Setting)
                {
                    $settings_array[$Setting->getSettingKey()] = $Setting;
                }       
               
                if($Request->isMethod('POST'))
                {
                    $form_data = $Request->request->all();
                    $EntityManager = $Doctrine->getManager();            
                    foreach($form_data as $key=>$value)
                    {
                        if(!isset($settings_array[$key]))
                        {
                            $Setting = new Setting();
                            $Setting->setSettingKey($key);
                            $settings_array[$key] = $Setting;                    
                        }
                        else 
                        {
                            $Setting = $settings_array[$key];
                        }

                        $Setting->setValue($value);               
                        $EntityManager->persist($Setting);                   

                    }
                    $EntityManager->flush();
                    $messages[] = ViewMessage::constructMessage('Settings updated.', 'success');
                    $Session->set('messages', $messages);
                    return $this->redirect($this->generateUrl('admin_settings', ['template'=>$template]));
                }

                return $this->render("admin/settings/$template.html.twig", [
                    'settings'=>$settings_array
                ]);
            },
            $this->generateUrl('admin_settings', ['template'=>$template])
        );
    }

    public function preRenderRoute() {}
}
